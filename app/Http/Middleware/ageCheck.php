<?php

namespace App\Http\Middleware;

use Closure;

class ageCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //echo 'This is from Middleware';
        if($request->age and $request->age<18)
        {
            return redirect('video14Noaccess');
        }
        return $next($request);
    }
}
