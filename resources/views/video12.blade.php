<h1>Login form page</h1>

<form action="video12" method="Post">
    @csrf   <!--Ce "csrf permet de generer un token pour la securite. Il est necesssaire pour afficher les infos renseignees "-->
    <input type="text" name="username" placeholder="enter your id" ><br>
    <Span style="color:red">
        @error('username')
            {{$message}}
        @enderror
    </Span>
    <br>
    <input type="password" name="userpassword" placeholder="enter your passeword" ><br>
    <Span style="color:red">
        @error('userpassword')
            {{$message}}
        @enderror
    </Span>
    <br>
    <button type="submit">Login</button>
</form>